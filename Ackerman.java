import java.math.BigInteger;
public class Ackerman {
    public static void main(String[] args) {
        BigInteger first = new BigInteger("5");
        BigInteger second = new BigInteger("0");
        System.out.println("Ackerman of (" + first + ", " + second + "): " + ack(first,second));
    }
    public static BigInteger ack(BigInteger m, BigInteger n) {
        return m.equals(BigInteger.ZERO)
                ? n.add(BigInteger.ONE)
                : ack(m.subtract(BigInteger.ONE),
                            n.equals(BigInteger.ZERO) ? BigInteger.ONE : ack(m, n.subtract(BigInteger.ONE)));
    }
}
